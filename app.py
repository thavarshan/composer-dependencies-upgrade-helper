from src.package_updator import PackageUpdator


def main():
    updator = PackageUpdator('modified_files/composer.json')
    updator.update()


if __name__ == '__main__':
    main()
