import re
import json
from .packagist import Packagist


class PackageUpdator:

    package_dependencies = {}
    extensions = {}

    def __init__(self, composer_file):
        self.composer_file = composer_file
        self.packages = self.get_packages()

    def update(self):
        composer_data = self.get_composer_data()
        complete_requirements = {
            **self.extensions,
            **self.find_updatable_packages()
        }
        composer_data.update({'require': complete_requirements})

        with open(self.composer_file, 'w+') as write_file:
            json.dump(composer_data, write_file, indent=4)

    def find_updatable_packages(self):
        packages = self.packages
        packagist = Packagist()

        for package in packages:
            version = packages.get(package).replace('^', '')
            latest_version = packagist.get_latest_package_version(package)

            if version < latest_version:
                packages[package] = f'^{latest_version}'

        return packages

    def get_packages(self):
        dependencies = self.get_all_dependencies()

        for dependency in dependencies:
            if not re.match('(ext)', dependency) and not re.match('(php)', dependency):
                self.package_dependencies[dependency] = dependencies[dependency]
            else:
                self.extensions[dependency] = dependencies[dependency]

        return self.package_dependencies

    def get_all_dependencies(self):
        return self.get_composer_data().get('require')

    def get_composer_data(self):
        with open(self.composer_file, 'r+') as json_file:
            return json.load(json_file)
