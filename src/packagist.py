import re
import requests


class Packagist:

    def get_latest_package_version(self, name):
        # '0' is just a place holder to prevent `max` function from throwing a `ValueError`
        numeric_versions = ['0']

        for version in self.get_package_versions(name):
            version = version.replace('v', '')
            if not re.search('([a-zA-Z])+', version):
                numeric_versions.append(version)

        return max(numeric_versions)

    def get_package_versions(self, name):
        return list(self.get_package_info(name).get('versions').keys())

    def get_package_info(self, name):
        return (
            requests.get(self.create_package_link(name))
            .json()
            .get('package')
        )

    def create_package_link(slef, name):
        return f'https://packagist.org/packages/{name}.json'
